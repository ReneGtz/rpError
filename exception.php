<?php
namespace rp {
    
    use Throwable;
    
    class exception extends \Exception {
    
        protected string $errorCode;
        
        protected string $logMsg;
        
        protected string $publicMsg;
        
        protected int $httpStateCode;
        
        protected ?Throwable $previous;
    
        /**
         * exception constructor.
         * @param string $errorCode
         * @param string $logMsg
         * @param string $publicMsg
         * @param int $httpStatusCode
         * @param self|null $previous
         */
        public function __construct( string $errorCode, string $logMsg = "", string $publicMsg = 'Internal Error', int $httpStatusCode = 500, self $previous = null ){
            $this->errorCode = $errorCode;
            $this->logMsg = $logMsg;
            $this->publicMsg = $publicMsg;
            $this->httpStateCode = $httpStatusCode;
            $this->previous = $previous;
            parent::__construct( $publicMsg, 0, $previous );
        }
        
        public function getErrorCode() :string {
        	return $this->errorCode;
        }
        
        public function getPublicMsg() :string {
            return $this->publicMsg;
        }
        
        public function getLogMsg() :string {
            return $this->logMsg;
        }
        
        public function getHttpStatusCode() :int {
            return $this->httpStateCode;
        }
        
        public function getPreviousError()  :?self {
            return $this->previous;
        }
	
	    /**
	     * @param string $logMsg
	     * @param string|null $pubMsg
	     * @param int|null $httpStatusCode
	     * @return $this <b>rp\exception</b>
	     */
        public function extend( string $logMsg, ?string $pubMsg = null, int $httpStatusCode = null ) :self {
        	$this->previous = new self( $this->getErrorCode(), $this->getLogMsg(), $this->getPublicMsg(), $this->getHttpStatusCode(), $this->getPreviousError() );
        	$this->logMsg = $logMsg;
        	if( $pubMsg !== null ){
        		$this->publicMsg = $pubMsg;
	        }
        	if( $httpStatusCode !== null ){
        		$this->httpStateCode = $httpStatusCode;
	        }
        	return $this;
        }
	
	    /**
	     * @return error <b>rp\error</b>
	     */
        public function getAsError() :error {
        	return $this->_getAsError( $this );
        }
	
	    /**
	     * @param exception $exception
	     * @return error <b>rp\error</b>
	     */
        private function _getAsError( self $exception ) :error {
        	if( $exception->getPreviousError() !== null ){
        		$newPrevious = $this->_getAsError( $exception->getPreviousError() );
	        } else {
        		$newPrevious = null;
	        }
        	
        	return new error( $this->getErrorCode(), $this->getLogMsg(), $this->getPublicMsg(), $this->getHttpStatusCode(), $newPrevious );
        }
	
	    /**
	     * @param string $logMsg
	     * @param string|null $pubMsg
	     * @param int|null $httpStatusCode
	     * @return error <b>rp\error</b>
	     */
        public function extendAsError( string $logMsg, ?string $pubMsg = null, int $httpStatusCode = null ) :error {
        	$this->extend( $logMsg, $pubMsg, $httpStatusCode );
        	return $this->getAsError();
        }
    }
}