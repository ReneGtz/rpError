if( typeof rp  === 'undefined' ){
	let rp = {};
}

rp.error = class {
	/** @type { string } */
	#id;

	/** @type { string } */
	#pubMsg;

	/** @type { string } */
	#logMsg;

	/** @type { rp.error|null } */
	#previous;

	/** @type { int } */
	static get id(){
		return 0;
	}

	/** @type { int } */
	static get pubMsg(){
		return 1;
	}

	/** @type { int } */
	static get logMsg(){
		return 2;
	}

	/** @type { int } */
	static get previous(){
		return 3;
	}

	/**
	 * @param id { string }
	 * @param logMsg { string }
	 * @param pubMsg { string }
	 * @param previous { rp.error|null }
	 */
	constructor( id, logMsg, pubMsg = 'InternalError', previous = null ){
		this.#id = id;
		this.#logMsg = logMsg;
		this.#pubMsg = pubMsg;
		this.#previous = previous;
	}

	/**
	 * @return {string}
	 */
	getID(){
		return this.#id;
	}

	/**
	 * @return {string}
	 */
	getPubMsg(){
		return this.#pubMsg;
	}

	/**
	 * @return {string}
	 */
	getLogMsg(){
		return this.#logMsg;
	}

	/**
	 * @return { rp.error|null }
	 */
	getPrevious(){
		return this.#previous;
	}

	/**
	 * @param logMsg { string }
	 * @param pubMsg { string|null }
	 * @return { rp.error }
	 */
	extend( logMsg, pubMsg = null ){
		let toRet = this.#previous = new rp.error( this.#id, this.#logMsg, this.#pubMsg, this.#previous );
		this.#logMsg = logMsg;
		if( pubMsg !== null ){
			this.#pubMsg = pubMsg;
		}
		this.#previous = toRet;
		return toRet;
	}
};
