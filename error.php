<?php
namespace rp;

class error {
    
    private string $id;
    private string $logMsg;
    private string $pubMsg;
    private int $httpStatusCode;
    private ?self $previous;
    
    /**
     * error constructor.
     * @param string $id
     * @param string $logMsg
     * @param string $pubMsg
     * @param int $httpStatusCode
     * @param error|null $previous
     */
    public function __construct( string $id, string $logMsg, string $pubMsg = 'Internal Error', int $httpStatusCode = 500, ?self $previous = null ){
        $this->id = $id;
        $this->logMsg = $logMsg;
        $this->pubMsg = $pubMsg;
        $this->httpStatusCode = $httpStatusCode;
        $this->previous = $previous;
    }
    
    /**
     * @return string
     */
    public function getPubMsg() : string {
        return $this->pubMsg;
    }
    
    /**
     * @return string
     */
    public function getLogMsg() : string {
        return $this->logMsg;
    }
    
    /**
     * @return string
     */
    public function getId() : string {
        return $this->id;
    }
    
    /**
     * @return int
     */
    public function getHttpStatusCode() : int {
        return $this->httpStatusCode;
    }
    
    /**
     * @return self|null
     */
    public function getPrevious() : ?self {
        return $this->previous;
    }
    
    /**
     * extend the error information
     * 
     * @param string $logMsg the new part of the log msg to save
     * @param string|null $pubMsg the new message to show to the user
     * @param int|null $httpStatusCode the new HTTP status code
     * @return $this <b>rpError</b>
     */
    public function extend( string $logMsg, ?string $pubMsg = null, int $httpStatusCode = null ) :self {
        $toRet = new self( $this->id, $this->logMsg, $this->pubMsg, $this->httpStatusCode, $this->previous );
        $this->logMsg = $logMsg;
        if( $pubMsg !== null ){
            $this->pubMsg = $pubMsg;
        }
        if( $httpStatusCode !== null ){
            $this->httpStatusCode = $httpStatusCode;
        }
        $this->previous = $toRet;
        return $toRet;
    }
	
	/**
	 * get the given error as exception
	 * @param error $error
	 * @return exception <b>rp\exception</b>
	 */
    private function _getAsException( self $error ) :exception {
	    if( $error->previous !== null ){
		    $newPrevious = $this->_getAsException( $error->previous );
	    } else {
	    	$newPrevious = null;
	    }
	    return new exception( $error->getId(), $error->getLogMsg(), $error->getPubMsg(), $error->getHttpStatusCode(), $newPrevious );
    }
	
	/**
	 * get its self as an error
	 * @return exception <b>rp\exception</b>
	 */
    public function getAsException() :exception {
    	return $this->_getAsException( $this );
    }
	
	/**
	 * @param string $logMsg
	 * @param string|null $pubMsg
	 * @param int|null $httpStatusCode
	 * @return exception <b>rp\exception</b>
	 */
    public function extendAsException( string $logMsg, ?string $pubMsg = null, int $httpStatusCode = null ) :exception {
    	$this->extend( $logMsg, $pubMsg, $httpStatusCode );
    	return $this->getAsException();
    }
    
}