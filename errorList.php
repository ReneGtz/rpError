<?php
namespace rp {
	class errorList {
	    
        /** @var string  */
		const ERROR_CODE = 'code';
		const MSG_LOG = 'log';
		const MSG_PUB = 'msg';
		const PREVIOUS = 'previous';
		const STATUS_CODE = 'state';

		static array $errorInfo = array();
  
		private function __construct(){}
		
        /**
         * @param string $errorCode the error code for the occurred error ( see class.rpErrorList.php for a list of already existing )
         * @param string|null $logMsg the message that should be saved in the log files or log tables
         * @param string $publicMsg the message that is save to show to the user in the frontend.
         * @param int $state_code the http status code that may be used for the response
         * @return array
         */
		static public function &writeNewError( string $errorCode, ?string $logMsg = null, string $publicMsg = 'Internal Error', int $state_code = 500 ) :array {
			self::$errorInfo = array(
				self::ERROR_CODE => $errorCode,
				self::MSG_LOG => $logMsg,
				self::MSG_PUB => $publicMsg,
				self::STATUS_CODE => $state_code,
				self::PREVIOUS => null
			);
			return self::$errorInfo;
		} //end function writeNewError()
		
		static public function extendLastError( string $logMsg, ?string $publicMsg = 'Internal Error', int $state_code = 500 ){
			if( empty( self::$errorInfo ) ){
				if( $logMsg === null ){
					$logMsg = 'No error msg';
				}
				self::writeNewError( '000-000-000', $logMsg, $publicMsg );
			
			} else {
				$error = &self::getLastError();
				
				self::$errorInfo = array(
                    self::ERROR_CODE => $error[ self::ERROR_CODE ],
                    self::MSG_LOG => $error[ self::MSG_LOG ],
                    self::MSG_PUB => $publicMsg,
                    self::STATUS_CODE =>$state_code,
                    self::PREVIOUS => $error
                );
			}
		} //end function extendLastError()
		
		static public function &getLastError() :?array{
			if( empty( self::$errorInfo ) ){
				$null = null;
				return $null;
			}
			
			return self::$errorInfo;
		} //end function getLastError()
		
		static public function getErrorWithoutLogMsg( array $error = null ) :array {
		    if( empty( $error ) ){
		        return array();
            }
            
			$toRet = array(
				self::ERROR_CODE => $error[ self::ERROR_CODE ],
				self::MSG_PUB => $error[ self::MSG_PUB ]
			);
			if( $error[ self::PREVIOUS ] !== null ){
				$toRet[ self::PREVIOUS ] = self::getErrorWithoutLogMsg( $error[ self::PREVIOUS ] );
			}
			return $toRet;
		} //end function getErrorWithoutLogMsg()
		
        
        static public function getAllErrorsWithoutLogMsg() :array {
		    $toRet = array();
		    foreach( self::$errorInfo AS $error ){
		        $toRet[] = self::getErrorWithoutLogMsg( $error );
            }
		    return $toRet;
        }
        
        static public function getErrorCount() :int {
			return count( self::$errorInfo );
        }
        
		/**
		 * get the last error ID set from writeNewError()
		 *
		 * @return string|null <b>string</b> the error code<br/><b>null</b> if no error is set
		 */
        static public function getLastErrorCode() :?string {
			$c = count( self::$errorInfo );
			if( $c < 1 ){
				return null;
			}
			
	        return self::getLastErrorCodeFromErrorArray( self::$errorInfo[$c - 1 ] );
        } //end function getLastErrorCode()
		
		/**
		 * get the error code from the given error array
		 *
		 * @param array $errorArray an array, written by writeNewError()
		 * @return string|null <b>string</b> the error code<br/><b>null</b> if no error code is set
		 */
		static private function getLastErrorCodeFromErrorArray( array $errorArray ) :?string {
			if( $errorArray[ self::ERROR_CODE ] !== null ){
				return $errorArray[ self::ERROR_CODE ];
			}
			if( $errorArray[ self::PREVIOUS ] !== null ){
				return self::getLastErrorCodeFromErrorArray( $errorArray[ self::PREVIOUS ] );
			}
			return null;
		} //end function _getLastErrorCode()
		
		static public function getErrorForResponse() :array {
			$c = count( self::$errorInfo );
			if( $c < 1 ){
				return array();
			}
			
			$lastError = self::$errorInfo[$c - 1 ];
			
			return array(
				self::ERROR_CODE => $lastError[ self::ERROR_CODE ],
				self::MSG_PUB => $lastError[ self::MSG_PUB ]
			);
		}
		
		public static function createAsException() :?exception {
		    if( empty( self::$errorInfo )){
		        return null;
            }
		    
		    return self::_createAsException( self::$errorInfo );
        }
        
        private static function _createAsException( ?array $data ) :?exception {
		    if( $data === null ){
		        return null;
            }
    
            $previous = self::_createAsException( $data[ self::PREVIOUS ] );
		    return new exception( $data[ self::ERROR_CODE ], $data[ self::MSG_LOG ], $data[ self::MSG_PUB ], $data[ self::STATUS_CODE ], $previous );
        }
        
        public static function createNewException( string $errorCode, string $logMsg = "", string $publicMsg = 'Internal Error', int $httpStatusCode = 0 ) :exception {
		    return new exception( $errorCode, $logMsg, $publicMsg, $httpStatusCode, null );
        }
		
	} //end class
} //end namespace