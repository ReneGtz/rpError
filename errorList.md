#List of existing Error-codes

|      ID      | Language |  Project name   |    description     |
| ------------ | -------- | --------------- | ------------------ |
| 000-000-001  |          |                 | Unknown request id |
| 000-000-002  |          |                 | Error while setting up database connection in main-api-class |
| 000-000-003  |          |                 | Failed to gain account list in dbLogic |
| 000-000-004  |          |                 | SQL statement execution failed for getAccounts() |
| 000-000-005  |          |                 | parameter "page" was omitted on api call mail::getAccountList()  |
| 000-000-006  |    JS    |                 | failed to send request to main.php |
| 000-000-007  |    JS    |                 | getAccountList: Error unexpected occurred while ajax request |
| 000-000-008  |    JS    | rpMessengerTool | getRoutingList: unexpected error occurred while ajax request |
| 000-000-009  |    JS    | rpMessengerTool | failed to send request to main.php in "getRoutingList" |
| 000-000-00A  |    JS    | rpMessengerTool | ajax http code |
| 000-000-00B  |    PHP   | rpMessengerTool | main API -> getRoutingList(). Parameter $page not set |
| 000-000-00C  |    PHP   | rpMessengerTool | routing dbInterface eventLog. Execution of query failed |
| 000-000-00D  |    PHP   | rpMessengerTool | routing dbLogic getRoutingList() failed to load routing list from event-log |
| 000-000-00E  |    PHP   | rpMessengerTool | account dbLogic _getAccountList() query failed |
| 000-000-00F  |    PHP   | rpMessengerTool | account dbLogic _getAccountList() query failed with exception |
| 000-000-00G  |    PHP   |      rpPDO      | failed to apply definition on result. |
| 000-000-00H  |    PHP   |      rpPDO      | failed with exception |
| 000-000-00I  |    PHP   |      rpPDO      | fetch failed with. |
| 000-000-00J  |    PHP   |      rpPDO      | unknown result mode |
| 000-000-00K  |    PHP   |                 | modules\database\tableLogic\message no such message id found for search in eventLog |
| 000-000-00L  |    PHP   |                 | modules\database\tableLogic\message no account to customer relationship found |
| 000-000-00M  |    PHP   |                 | modules\database\tableLogic\message no events found for account and customer |
| 000-000-00N  |    PHP   |                 | getMessageIDFromEventDetails encountered a non numeric value als message ID in a routing block |
| 000-000-00O  |    PHP   |                 | getMessageIDFromEventDetails encountered a not supportet variable type |
| 000-000-00P  |    PHP   |                 | getEventLogByMessageID() failed to find a message ID in the routing block |
| 000-000-00Q  |    PHP   |                 | getAssignedAgentIdFromEventDetails unexpected last routing block / behavior |
| 000-000-00R  |    PHP   |                 | decodeApiLogJson failed to decode json string from ApiLog |
| 000-000-00S  |    PHP   |                 | decodeApiLogJson json dose not contain a parameter key |
| 000-000-00T  |    PHP   |                 | decodeApiLogJson encountered a invalid type for the parameter key |
| 000-000-00U  |    PHP   |                 | getLogoutOfOSP failed to gain accountID from osp user |
| 000-000-00V  |    PHP   |                 | getLogoutOfOSP no such osp user found |
| 000-000-00W  |    PHP   |                 | getAPiKeyByAccountUserID no account found for account user ID |
| 000-000-00X  |    PHP   |                 | getApiKeyByAccountID no account found |
| 000-000-00Y  |    PHP   |                 | getApiKeyByAccountGroupID no account-group found |
| 000-000-00Z  |    PHP   |                 | getApiKeyByAccountGroupID no api key id found for account group |
| 000-000-010  |    PHP   |                 | getCustomers only one ( getCustomers or $offset ) was set |
| 000-000-011  |    PHP   |                 | getCustomerList $limit is an unexpected format. |
| 000-000-012  |    PHP   |                 | getCustomerList $offset is an unexpected format. |
| 000-000-013  |    PHP   |                 | getLogoutOfAccountUser failed to gain event log because no account with id was found |
| 000-000-014  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to gain event log because no api key is set for account group |
| 000-000-015  |    PHP   |                 | getLoginAndLogoutOfApiKeyID no apikey with id found |
| 000-000-016  |    PHP   |                 | getLoginAndLogoutOfAccount no account ground with id |
| 000-000-017  |    PHP   |                 | getAccountUserLoginAndLogout sql execute error |
| 000-000-018  |    PHP   |                 | getUserFromLoginLogoutEntry fist parameter is missing required array key |
| 000-000-019  |    PHP   |                 | getUserFromLoginLogoutEntry no parameter in post information found |
| 000-000-020  |    PHP   |                 | getUserFromLoginLogoutEntry no account user or osp user found |
| 000-000-021  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to get event-log of account-group |
| 000-000-022  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to ensure that event is from account group |
| 000-000-023  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to ensure that event is from account group |
| 000-000-024  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to ensure that event is from account group |
| 000-000-025  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to ensure that event is from account group |
| 000-000-026  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to ensure that event is from account group |
| 000-000-027  |    PHP   |                 | getLoginAndLogoutOfAccountGroup failed to ensure that event is from account group |
| 000-000-028  |    PHP   |                 | decodeResponse failed to decode json response |
| 000-000-029  |    PHP   |                 | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-02A  |    PHP   |                 | getByApiKeyAndEndPoint invalid date time string for starting point |
| 000-000-02B  |    PHP   |                 | getByApiKeyAndEndPoint invalid date time string for end date |
| 000-000-02C  |    PHP   |                 | getEventLogsByTargetAndCommandList no target set for filter |
| 000-000-02D  |    PHP   |                 | getEventLogsByTargetAndCommandList no command set for filter |
| 000-000-02E  |    PHP   |                 | decodeApiLogJson failed to decode string |
| 000-000-02F  |    PHP   | tRoMarketWatch  | Unknown endpoint class |
| 000-000-02G  |    PHP   | tRoMarketWatch  | Unknown endpoint function |
| 000-000-02H  |    PHP   | tRoMarketWatch  | Unknown endpoint |
| 000-000-02I  |    PHP   | tRoMarketWatch  | Multiple return declarations in phpDoc |
| 000-000-02J  |    PHP   | tRoMarketWatch  | unknown api parameter |
| 000-000-02K  |    PHP   | tRoMarketWatch  | Failed to gain type list for parameter |
| 000-000-02L  |    PHP   | tRoMarketWatch  | file for class dose not exists |
| 000-000-02M  |    PHP   | tRoMarketWatch  | failed to open file |
| 000-000-02N  |    PHP   | tRoMarketWatch  | failed to load size of file |
| 000-000-02O  |    PHP   | tRoMarketWatch  | failed to set file-pointer END |
| 000-000-02P  |    PHP   | tRoMarketWatch  | failed to set file-pointer Start |
| 000-000-02Q  |    PHP   | tRoMarketWatch  | failed to get content of file |
| 000-000-02R  |    PHP   | tRoMarketWatch  | unexpected exception |
| 000-000-02S  |    PHP   | tRoMarketWatch  | failed to find end of parameter declaration |
| 000-000-02T  |    PHP   | tRoMarketWatch  | unknown parameter declaration |
| 000-000-02U  |    PHP   | tRoMarketWatch  | unknown parameter declaration type |
| 000-000-02V  |    PHP   | tRoMarketWatch  | unknown parameter declaration variable name |
| 000-000-02W  |    PHP   | tRoMarketWatch  | docBlock contain not declared parameter |
| 000-000-02X  |    PHP   | tRoMarketWatch  | parameter type miss match |
| 000-000-02Y  |    PHP   | tRoMarketWatch  | directory does not exist or is no directory |
| 000-000-02Z  |    PHP   | tRoMarketWatch  | file can not be open to read |
| 000-000-030  |    PHP   | tRoMarketWatch  | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-031  |    PHP   | tRoMarketWatch  | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-032  |    PHP   | tRoMarketWatch  | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-033  |    PHP   | tRoMarketWatch  | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-034  |    PHP   | tRoMarketWatch  | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-035  |    PHP   | tRoMarketWatch  | getLoginAndLogoutOfAccount failed to ensure that event is from account |
| 000-000-036  |    PHP   |     rpCurl      | not a valid URL |
| 000-000-037  |    PHP   | tRoMarketWatch  | convertMessageToPlainText failed to decode base64 encoded message content |
| 000-000-038  |    PHP   | tRoMarketWatch  | getMessageDetailByID to many results with one id |
| 000-000-039  |    PHP   | tRoMarketWatch  | failed to write file |
| 000-000-03A  |    PHP   | tRoMarketWatch  | unexpected exception |
| 000-000-03B  |    PHP   | tRoMarketWatch  | failed to write file |
| 000-000-03C  |    PHP   | tRoMarketWatch  | parse apiDef failed |
| 000-000-03D  |    PHP   | tRoMarketWatch  | class endpoint dose not exists |
| 000-000-03E  |    PHP   | tRoMarketWatch  | function endpoint dose not exists |
| 000-000-03F  |    PHP   | tRoMarketWatch  | rpPDOStatement unknown result mode |
| 000-000-03G  |    PHP   | tRoMarketWatch  | duplicated entry for item |
| 000-000-03H  |    PHP   | tRoMarketWatch  | reflection exception |
| 000-000-03I  |    PHP   | tRoMarketWatch  | failed to gain endpoint-class methods |
| 000-000-03J  |    PHP   | tRoMarketWatch  | name with namespace is shorter than the namespace alone |
| 000-000-03K  |    PHP   | tRoMarketWatch  | duplicated endpoint class name |
| 000-000-03L  |    PHP   | tRoMarketWatch  | Invalid value for parameter |
| 000-000-03M  |    PHP   | tRoMarketWatch  | Call to unknown endpoint class |
| 000-000-03N  |    PHP   | tRoMarketWatch  | Missing required parameter |
| 000-000-03O  |    PHP   | tRoMarketWatch  | Call to unknown endpoint function |
| 000-000-03P  |    PHP   | tRoMarketWatch  | Unexpected reflection exception |
| 000-000-03Q  |    PHP   | tRoMarketWatch  | Invalid default valueguten morgen, |
| 000-000-03R  |    PHP   | tRoMarketWatch  | Unexpected exception while calling endpoint |
| 000-000-03S  |    PHP   | tRoMarketWatch  | Unexpected exception in apiV1 |
| 000-000-03T  |    PHP   | tRoMarketWatch  | Invalid endpoint |
| 000-000-03U  |    PHP   | tRoMarketWatch  | unknown error |
| 000-000-03V  |    PHP   | tRoMarketWatch  | unknown default value |
| 000-000-03W  |    PHP   | tRoMarketWatch  | no item with this criteria found |
| 000-000-03X  |    PHP   | tRoMarketWatch  | to many entries found for item with given criteria |
| 000-000-03Y  |    PHP   |      rpApi      | php-doc declare a type-of-parameter but no array was declared inline |
| 000-000-03Z  |    PHP   |      rpApi      | type %s was mentioned in php-doc but not declared |
| 000-000-040  |    PHP   |      rpApi      | type %s was declared but not mentioned in the php-doc |
| 000-000-041  |    PHP   |      rpApi      | Unsupported datatype %s for return value |
| 000-000-042  |    PHP   |      rpApi      | Call to a unknown endpoint %s::%s |
| 000-000-043  |    PHP   |      rpApi      | Unsupported datatype %s for parameter %s |
| 000-000-044  |    PHP   |      rpApi      | multiple return values declared in docBlock for method %s |
| 000-000-045  |    PHP   |      rpApi      | Unsupported datatype %s for return value |
| 000-000-046  |    PHP   |      rpApi      | Multiple description definition was found in "%s" |
| 000-000-047  |    PHP   |      rpApi      | Unsupported datatype %s for parameter %s |
| 000-000-048  |    PHP   |      rpApi      | Class %s does not contain called api function %s |
| 000-000-049  |    PHP   |      rpApi      | Call to unknown endpoint %s::%s |
| 000-000-04A  |    PHP   |      rpApi      | Call to unknown endpoint %s::%s. But the check sad it existed |
| 000-000-04B  |    PHP   |      rpApi      | Missing mandatory parameter %s |
| 000-000-04C  |    PHP   |      rpApi      | unrecognised type "%s" in function "checkDataType |
| 000-000-04D  |    PHP   |      rpPDO      | Unexpected error while creating rpPDO instance, with msg "%s" |
| 000-000-04E  |    PHP   |      rpCurl     | invalid request-body-type found for content-type |
| 000-000-04F  |    PHP   |      rpCurl     | Failed to set request body |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
